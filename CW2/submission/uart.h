#ifndef UART_H__
#define UART_H__

extern void uart_init();
extern char uart_get();
extern void uart_put(char x);

#endif
