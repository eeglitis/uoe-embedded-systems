#include "button.h"
#include "MK70F12.h"


// Initialize the buttons to operate and trigger interrupts
void button_init()
{
	// IRQs of buttons
	int IRQ_port_D = 90;
	int IRQ_port_E = 91;

	// Get their indices in the IPR and non-IPR registers
	int D_in_non_IPR = IRQ_port_D % 32;
	int D_index_non_IPR = IRQ_port_D / 32;
	int E_in_non_IPR = IRQ_port_E % 32;
	int E_index_non_IPR = IRQ_port_E / 32;

	// Enable button interrupts
	NVIC_ISER(D_index_non_IPR) |= (1 << D_in_non_IPR);
	NVIC_ISER(E_index_non_IPR) |= (1 << E_in_non_IPR);

	// Enable clock gating to the button pin ports
	SIM_SCGC5 |= SIM_SCGC5_PORTD_MASK;
	SIM_SCGC5 |= SIM_SCGC5_PORTE_MASK;

	// Configure the button pins to be GPIOs
	PORTD_PCR0  = PORT_PCR_IRQC(9) | PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK | PORT_PCR_PE_MASK | PORT_PCR_PS_MASK;
	PORTE_PCR26 = PORT_PCR_IRQC(9) | PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK | PORT_PCR_PE_MASK | PORT_PCR_PS_MASK;
}

// Read the value of the button with specified ID
int button_read(int BUTTON_ID)
{
	// It is either the button on port D
	if (BUTTON_ID == BUTTON_D)
		return GPIOD_PDIR;

	// Or the one on port E
	if (BUTTON_ID == BUTTON_E)
		return GPIOE_PDIR;
}
