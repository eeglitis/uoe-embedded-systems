#ifndef BUTTON_H__
#define BUTTON_H__

#define BUTTON_D 0
#define BUTTON_E 1

// Initialize the buttons to operate and trigger interrupts
extern void button_init();
// Read the value of the button with specified ID
extern int button_read(int BUTTON_ID);

#endif
